plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/check_i2c_hid
command:
 set -x
 if sudo dmidecode -t 3 | grep -i "Notebook\|Laptop"; then
    dmesg | grep i2c | grep input || exit 1
 else
    echo "this machine is not laptop, no need to check i2c touchpad"
 fi
_summary: check if i2c device exists
_description:
 check if i2c device exists

plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/intel_pmc_core
requires:
 sleep.mem_sleep == 's2idle'
 cpuinfo.type == 'GenuineIntel'
user: root
command:
 if [ ! -d /sys/kernel/debug/pmc_core ]; then
     echo "The system has no /sys/kernel/debug/pmc_core."
     echo "It is required for doing low power idle analysis."
     echo "For more detail, please refer to https://01.org/blogs/qwang59/2020/linux-s0ix-troubleshooting"
     exit 1
 fi
_summary: Check if intel_pmc_core driver is probed
_description:
 For system support low power idle, intel_pmc_core
 driver is required for all Intel platforms.

unit: template
template-resource: device
template-filter:
 device.path == '/devices/pci0000:00/0000:00:1f.0' and device.vendor_id == '32902'
 device.product_id >= '17280' and device.product_id <= '17311'
template-unit: job
plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/tgp-rid-check_{product_id}
command:
 rid_hex=$(lspci -xs 00:1f.0 | grep "00: " | cut -d " " -f10)
 rid="0x$rid_hex"
 if [[ $rid -eq 0x10 ]]; then
     echo "The PCH has known issue, the detail is in lp:1909053."
     echo "Please request another hardware to proceed the test."
     exit 1
 fi
_summary: PCH revision ID check
_description:
 The specific PCH revision ID has known issue, add the test case
 for filtering out the invalid hardware.

unit: template
template-resource: device
template-filter:
 device.category == 'TOUCHPAD'
 device.vendor_id == '1160'
 device.product_id == '4618'
template-unit: job
plugin: shell
category_id: com.canonical.plainbox::miscellanea
id: miscellanea/touchpad-firmware-version_{product_slug}
user: root
requires:
 package.name == 'i2c-tools'
 dmi.sku in ['0A69','0A6A','09C3','09C4']
command:
 out=$(get-tp-fw-ver.sh)
 major=$(echo "$out" | cut -d "," -f1 | cut -d " " -f2)
 minor=$(echo "$out" | cut -d "," -f2 | cut -d " " -f3)
 version=$(echo "$out" | cut -d "," -f3 | cut -d " " -f3)
 # shellcheck disable=SC2050
 if [ "{vendor_id}" = "1160" ] && [ "{product_id}" = "4618" ]; then
     if [[ $major -lt 0x05 ]] || [[ $minor -lt 0x34 ]]; then
         echo "The touchpad firmware version is $version which is not correct."
         echo "please refer to the lp:1903951 for getting correct firmware."
         exit 1
     fi
 fi
_summary: Touchpad firmware version checking
_description:
 The test case is for gating invalid touchpad firmware.
